package ie.lyit.studentattendance;

/**
 * Created by barry on 07/02/2017.
 *
 * Immutable Class Constants
 *
 */
class Constants {
    private static final String validateUserUrl = "http://52.26.52.79/validateUser.php";
    private static final String recordAttendance = "http://52.26.52.79/recordAttendance.php";

    static String getValidateUserUrl() {
        return validateUserUrl;
    }

    static String getRecordAttendanceUrl(){return recordAttendance;}

}
