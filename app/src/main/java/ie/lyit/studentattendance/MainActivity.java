package ie.lyit.studentattendance;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    // DECLARE INSTANCE VARIABLES //
    private ProgressDialog progressDialog;
    private static final String TAG = "ValidateUser";

    private EditText editTextUsername;
    private EditText editTextPassword;
    private Button buttonLogin;

    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        buttonLogin = (Button) findViewById(R.id.buttonLogin);

        buttonLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            //On click function
            public void onClick(View view) {
                //*****************************************************************//
                //******* CHECK VALUES HAVE BEEN ENTERED IN BOTH FIELDS ***********//
                //*****************************************************************//
               if (editTextUsername.getText().toString().equals("") || editTextPassword.getText().toString().equals("")) {

                        Toast.makeText(getApplicationContext(), "Error, Need to enter username AND password.",
                                Toast.LENGTH_SHORT).show();// TELL USER TO ENTER DATA INTO BOTH FIELDS

                        return;
                    }

                else{

                   String message = editTextUsername.getText().toString();

                   //********************************************************************//
                   //  CALL LOGIN USER METHOD PASSING IN URL, USERNAME AND PASSWORD     //
                   //******************************************************************//
                   loginUser(MainActivity.this, Constants.getValidateUserUrl(),
                          editTextUsername.getText().toString().trim(), editTextPassword.getText().toString().trim());

               }


            } });}
    private void loginUser(final Context context, final String getValidateUserUrl, final String name, final String password) {

        if (DBMethods.isOnline(context)) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Checking Login Details...");
            progressDialog.show();

            StringRequest req = new StringRequest(Request.Method.POST, getValidateUserUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            if (response.trim().equalsIgnoreCase("true")) {
                                Toast.makeText(context, "Logged In successfully.",
                                        Toast.LENGTH_SHORT).show();
                                finish();//close activity
                                Intent myIntent = new Intent(MainActivity.this,ScanTag.class);
                                myIntent.putExtra("message",name);
                                startActivity(myIntent);

                            } else {
                                Toast.makeText(context, "Error.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    params.put("username", name);
                    params.put("password", password);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(req);
        } else {
            Toast.makeText(context, "Error, no internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }

    }


}
