package ie.lyit.studentattendance;

/**
 * Created by barry on 14/02/2017.
 */

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Reads an NDEF tag and sends a proximity event to AgoControl.
 */
public class NdefReaderTask extends AsyncTask<Tag, Void, String> {
    private static final String TAG = NdefReaderTask.class.getSimpleName();


    @Override
    protected String doInBackground(final Tag... params) {
        final Tag tag = params[0];
        final Ndef ndef = Ndef.get(tag);
        if (ndef == null) {
            // NDEF is not supported by this Tag.
            return null;
        }
        final NdefMessage ndefMessage = ndef.getCachedNdefMessage();
        final NdefRecord[] records = ndefMessage.getRecords();
        for (final NdefRecord ndefRecord : records) {
            if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN
                    && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                try {

                    final String text = readText(ndefRecord);

                    return text;
                } catch (final UnsupportedEncodingException e) {
                    Log.e(TAG, "Unsupported Encoding", e);
                }
            }
        }
        return null;
    }

    private String readText(final NdefRecord record) throws UnsupportedEncodingException {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */
        final byte[] payload = record.getPayload();
        // Get the Text Encoding
        final String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
        // Get the Language Code
        final int languageCodeLength = payload[0] & 0063;
        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
        // e.g. "en"
        // Get the Text
        return new String(payload,
                languageCodeLength + 1,
                payload.length - languageCodeLength - 1,
                textEncoding);
    }

    @Override
    protected void onPostExecute(final String result) {
        // Do nothing.
    }
}
