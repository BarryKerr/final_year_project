package ie.lyit.studentattendance;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class ScanTag extends AppCompatActivity {

    private ProgressDialog progressDialog;

    public static final String MIME_TEXT_PLAIN = "text/plain";
    public static final String TAG = "NfcDemo";

    private NfcAdapter mNfcAdapter;

    private TextView mTextView, txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_tag);

        Intent myIntent = getIntent();
        String myMessage = myIntent.getStringExtra("message");

        //--ADD MESSAGE FROM LOGIN SCREEN CONTAINING STUDENT_ID TO TEXTVIEW--//
        txtMessage = (TextView) findViewById(R.id.txtMessage);
        txtMessage.setText(myMessage);

        Button btnScanTag = (Button) findViewById(R.id.button);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mTextView = (TextView) findViewById(R.id.mTextView);

        if (mNfcAdapter != null) {
            mTextView.setText("Read an NFC tag");
        } else {
            mTextView.setText("This phone is not NFC enabled.");
        }


        handleIntent(getIntent());

        btnScanTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                registerAttendance(ScanTag.this, Constants.getRecordAttendanceUrl(),
                        txtMessage.getText().toString().trim()   , mTextView.getText().toString().trim());

            }

        });

    }
    @Override
    protected void onResume() {
        super.onResume();

        setupForegroundDispatch(this, mNfcAdapter);
    }

    @Override
    protected void onPause() {

        stopForegroundDispatch(this, mNfcAdapter);

        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
    }


    private void handleIntent(Intent intent)
    {
        String action = intent.getAction();


        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new AsyncNdefReaderTask().execute(tag);

            } else {
                Log.d(TAG, "Wrong mime type: " + type);
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {

            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = Ndef.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    new AsyncNdefReaderTask().execute(tag);
                    break;
                }
            }
        }
    }


    private class AsyncNdefReaderTask extends NdefReaderTask
    {
        @Override
        protected void onPostExecute(String result) {

            Log.d("onPostExecute", "onPostExecute");

            if (result != null) {
                mTextView.setText(result);
            }
        }
    }


    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter)
    {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }


    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }



    //------ALL THE CODE REQUIRED TO CONNECT TO THE DATABASE AND REGISTER THE STUDENTS ATTENDANCE---------------//
    private void registerAttendance(final Context context, final String getRecordAttendanceUrl, final String studentID, final String moduleID) {

        //make sure that the Database is online
        if (DBMethods.isOnline(context)) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Recording Attendance ");
            progressDialog.show();

            StringRequest req = new StringRequest(Request.Method.POST, getRecordAttendanceUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            if (response.trim().equalsIgnoreCase("true")) {
                                Toast.makeText(context, "Attendance Successfully Recorded",
                                        Toast.LENGTH_SHORT).show();
                                //**CLOSE THE ACTIVITY**//
                                finish();
                            } else {
                                Toast.makeText(context, "Error.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    params.put("username", studentID);
                    params.put("moduleID", moduleID);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(req);
        } else {
            Toast.makeText(context, "Error, no internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }

    }

}
